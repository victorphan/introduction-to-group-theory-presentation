\documentclass[xcolor=table]{beamer}
\usetheme{metropolis}
\usepackage[table, xcdraw]{xcolor}

\title{Introduction to Group Theory}
\subtitle{Based on Nathan Carter's \textit{Visual Group Theory}}
\author{Victor D. Phan}
\institute{Trustworthy Systems}
\date{\today}
\metroset{block=fill}
\setbeamertemplate{caption}{\raggedright\insertcaption\par}

\begin{document}

\frame{\titlepage}

\begin{frame}
    \frametitle{What is a group?}
    \begin{columns}
        \column{0.5\textwidth}
        \begin{figure} 
            \includegraphics[scale=0.3]{img/rubiks.png} 
        \end{figure}
        \column{0.5\textwidth}
        Basic Observations:
        \pause
        \begin{itemize}[<+- | alert@+>]
            \item There is a predefined list of moves that never changes
            \item Every move is reversible
            \item Every move is deterministic
            \item Moves can be combined in any sequence
        \end{itemize}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{What is a group?}
    \begin{block}{Informal definition of a group}
    A system or collection of actions satisfying the following rules:
    \begin{itemize}
        \item There is a predefined list of actions that never changes
        \item Every action is reversible
        \item Every action is deterministic
        \item Any sequence of consecutive actions is also an action
    \end{itemize}
    \end{block}
    \pause
    \begin{block}{Generators}
        Generators are the actions that generate all other actions.
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{The rectangle} 
    \begin{figure} 
        \includegraphics[scale=0.13]{img/cayley_rect.png}
    \end{figure}
    Vertical and horizontal flip are the generators of this
    group.
\end{frame}

\begin{frame}
    \frametitle{Two-light switch}
    \begin{figure} 
        \includegraphics[scale=0.16]{img/cayley_switch.png}
    \end{figure}
    \pause
    When two groups have the same structure, we say they are isomorphic.
\end{frame}

\begin{frame}
    \frametitle{Cayley diagrams and multiplication tables}
    \begin{columns}
        \column{0.5\textwidth}
        \begin{figure}
            \includegraphics[scale=0.1]{img/cayley_klein.png}
        \end{figure}
        Useful for representing groups as a collection of actions.
        \pause
        \column{0.5\textwidth}
        \begin{figure}
            \includegraphics[scale=0.1]{img/mult_klein.png}
        \end{figure}
        Useful for representing groups as a collection of binary operations.
    \end{columns}
    \pause
    \begin{block}{Isomorphism}
        Two groups that have the same Cayley diagram or multiplication table
        are isomorphic.
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Dihedral groups: symmetries of a triangle}
    \begin{columns}
        \column{0.5\textwidth}
        \begin{figure}
            \includegraphics[scale=0.1]{img/trans_tri.png}
        \end{figure}
        Rotating and flipping the triangle are the generators.
        \pause
        \column{0.5\textwidth}
        \begin{figure}
            \includegraphics[scale=0.1]{img/cayley_tri.png}
            \caption{Cayley diagram for \(D_3\)}
        \end{figure}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Dihedral groups: symmetries of a pentagon}
    \begin{columns}
        \column{0.5\textwidth}
        \begin{figure}
            \includegraphics[scale=0.12]{img/cayley_pent.png}
            \caption{Cayley diagram for \(D_5\)}
        \end{figure}
        \pause
        \column{0.5\textwidth}
        \begin{figure}
            \includegraphics[scale=0.13]{img/mult_pent.png}
            \caption{Multiplication table for \(D_5\)}
        \end{figure}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Permutations as groups}
    \begin{figure}
        \includegraphics[scale=0.1]{img/perm_eg.png}
    \end{figure}
    The collection of permutations satisfy the definition of a group.
    \pause
    \begin{figure}
        \includegraphics[scale=0.08]{img/perm_inv.png}
    \end{figure}
    \pause
    \begin{figure}
        \includegraphics[scale=0.11]{img/perm_comp.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Symmetric groups}
    We call the group of all permutation of \(n\) things the symmetric group
    \(S_n\).

    \pause
    \(S_3\) is the same as \(D_3\). What does \(S_4\) look like?
    \pause
    \begin{columns}
        \column{0.5\textwidth}
        \begin{figure}
            \includegraphics[scale=0.08]{img/cayley_s4_1.png}
        \end{figure}
        \column{0.5\textwidth}
        \begin{figure}
            \vspace{-22pt}
            \includegraphics[scale=0.09]{img/cayley_s4_2.png}
        \end{figure}
    \end{columns}
    It depends on the generators!
\end{frame}

\begin{frame}
    \frametitle{Permutation groups}
    \begin{figure}
        \includegraphics[scale=0.11]{img/cayley_a4.png}
    \end{figure}
    Taking these elements in \(S_4\) as generators produces a group with half
    the elements from \(S_4\).
    \pause
    \begin{block}{Definition of a subgroup}
        When one group is completely contained in another, the inner group is
        called a \textbf{subgroup} of the outer one.
    \end{block}
    Subgroups of symmetric groups are called permutation groups.
\end{frame}

\begin{frame}
    \frametitle{Cayley's Theorem: Statement}
    \begin{block}{Cayley's Theorem}
        Every finite group is isomorphic to a permutation group.
    \end{block}
    \pause
    Two approaches to construct a permutation group isomorphic to any finite
    group:
    \begin{itemize}
        \item Cayley diagram: get the generators
        \item Multiplication table: get all binary operations
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Cayley's Theorem: Cayley diagram}
    \begin{figure}
        \includegraphics[scale=0.16]{img/cayley_s3_thm.png}
    \end{figure}
    From a Cayley diagram of a group (left), we can construct the generators
    for the corresponding permutation group (right).
\end{frame}

\begin{frame}
    \frametitle{Cayley's Theorem: Multiplication Table}
    \begin{figure}
        \includegraphics[scale=0.16]{img/mult_v4_thm.png}
    \end{figure}
    From the multiplication table of a group (left), we can construct all the
    elements of the corresponding permutation group (right).
\end{frame}

\begin{frame}
    \frametitle{Cayley's theorem: multiplication table}
    \begin{figure}
        \includegraphics[scale=0.22]{img/mult_v4_perm.png}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Summary}
    \begin{itemize}
        \item Groups are useful for studying things with symmetry
        \item Cayley diagrams and multiplication tables are ways to represent
            groups
        \item Dihedral, symmetric and permutation groups
        \item Cayley's theorem
    \end{itemize}
\end{frame}

\end{document}
